import React from 'react';
import AppLoading from 'expo-app-loading';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useFonts } from 'expo-font';

// Auth Screen
import LoginScreen from './src/screens/Auth/LoginScreen';

// Main Screen
import HomeScreen from './src/screens/Main/HomeScreen';
import ProfileScreen from './src/screens/Main/ProfileScreen';
import BagScreen from './src/screens/Main/BagScreen';
import QuestScreen from './src/screens/Main/QuestScreen';
import MarketScreen from './src/screens/Main/MarketScreen';
import MapScreen from './src/screens/Main/MapScreen';
import FightScreen from './src/screens/Main/FightScreen';

const RomulusFonts = require('./src/assets/fonts/romulus.ttf');

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();

function DrawerNavigator() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={HomeScreen} />
      <Drawer.Screen name="Profile" component={ProfileScreen} />
      <Drawer.Screen name="Bag" component={BagScreen} />
      <Drawer.Screen name="Quest" component={QuestScreen} />
      <Drawer.Screen name="Market" component={MarketScreen} />
      <Drawer.Screen name="Map" component={MapScreen} />
      <Drawer.Screen name="Fight" component={FightScreen} />
    </Drawer.Navigator>
  );
}

export default function App() {
  const [fontsLoaded] = useFonts({
    Romulus: RomulusFonts,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Home" component={DrawerNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
