const constants = {
  HOST_URL: 'https://questionnaireapi.infinitereviews.org/',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
};

export default constants;