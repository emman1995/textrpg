import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';

const LoginScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ alignItems: 'center', margin: 10 }}>
        <Text style={styles.mainText}>Let’s Get Started</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.inputs}
          placeholder="Email"
          keyboardType="email-address"
          underlineColorAndroid="transparent"
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.inputs}
          placeholder="Password"
          secureTextEntry
          underlineColorAndroid="transparent"
        />
      </View>
      <TouchableHighlight
        style={[styles.buttonContainer, styles.signupButton]}
        onPress={() => navigation.navigate('Home')}
      >
        <Text style={styles.loginText}>Login</Text>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#242424',
    padding: 20,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
  },
  inputContainer: {
    borderColor: '#EBF0FF',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    borderWidth: 1,
    width: '100%',
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 10,
    borderColor: '#EBF0FF',
    flex: 1,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: '100%',
    borderRadius: 5,
  },
  mainText: {
    fontFamily: 'Romulus',
    color: '#fff',
    fontSize: 50,
  },
  loginText: {
    fontFamily: 'Romulus',
    color: '#fff',
    fontSize: 30,
  },
});

LoginScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

LoginScreen.defaultProps = {
  navigation: {},
};

export default LoginScreen;
