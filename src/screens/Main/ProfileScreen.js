import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  // Dimensions,
} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import PropTypes from 'prop-types';

// const { height, width } = Dimensions.get('window');

const barbarian = require('../../assets/character/barbarian/barbarian_1_walk.gif');

// https://simplemmo.fandom.com/wiki/Stats

const ProfileScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.playerContainer}>
        <Image style={styles.playerImage} source={barbarian} />
      </View>
      <View style={{ flex: 1 }}>
        <Row>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats, styles.statsValue]}>3000</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Atk</Text>
          </Col>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats, styles.statsValue]}>3000</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Atk</Text>
          </Col>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats, styles.statsValue]}>3000</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Atk</Text>
          </Col>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats, styles.statsValue]}>3000</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Str</Text>
          </Col>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats, styles.statsValue]}>3000</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Agi</Text>
          </Col>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats, styles.statsValue]}>3000</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Int</Text>
          </Col>
        </Row>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#242424',
  },
  playerContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  playerImage: {
    height: 200,
    width: 200,
  },
  playerName: {
    fontFamily: 'Romulus',
    fontSize: 30,
    color: '#fff',
  },
  menuButtonImage: {
    height: 80,
    width: 80,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stats: {
    background: 'pink',
    fontFamily: 'Romulus',
    color: '#fff',
    textAlign: 'center',
  },
  statsLabel: {
    fontSize: 25,
  },
  statsValue: {
    fontSize: 30,
  },
});

ProfileScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

ProfileScreen.defaultProps = {
  navigation: {},
};

export default ProfileScreen;
