import React, { useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';
import { Row, Col } from 'react-native-responsive-grid-system';

const tileOne = require('../../assets/menu/tile008.png');
const tileTwo = require('../../assets/menu/tile005.png');
const tileThree = require('../../assets/menu/tile010.png');
const tileFour = require('../../assets/menu/tile004.png');
const tileFive = require('../../assets/menu/tile006.png');
const tileSix = require('../../assets/menu/tile007.png');

const barbarian = require('../../assets/character/barbarian/barbarian_1_walk.gif');

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.playerContainer}>
        <Text style={styles.playerLevel}>Lvl 10</Text>
        <Image style={styles.playerImage} source={barbarian} />
        <Text style={styles.playerName}>Emman</Text>
        <TouchableOpacity> Attack </TouchableOpacity>
      </View>
      <View style={{ flex: 1 }}>
        <Row>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
              <Image style={styles.menuButtonImage} source={tileOne} />
              <Text style={styles.menuButtonLabel}>Profile</Text>
            </TouchableOpacity>
          </Col>
          <Col xs={4}>
            <TouchableOpacity onPress={() => navigation.navigate('Bag')}>
              <Image style={styles.menuButtonImage} source={tileTwo} />
              <Text style={styles.menuButtonLabel}>Bag</Text>
            </TouchableOpacity>
          </Col>
          <Col xs={4}>
            <TouchableOpacity onPress={() => navigation.navigate('Quest')}>
              <Image style={styles.menuButtonImage} source={tileThree} />
              <Text style={styles.menuButtonLabel}>Quest</Text>
            </TouchableOpacity>
          </Col>
          <Col
            xs={4}
            style={{
              backgroundColor: 'red',
            }}
          >
            <TouchableOpacity onPress={() => navigation.navigate('Market')}>
              <Image style={styles.menuButtonImage} source={tileFour} />
              <Text style={styles.menuButtonLabel}>Market</Text>
            </TouchableOpacity>
          </Col>
          <Col xs={4}>
            <TouchableOpacity onPress={() => alert()}>
              <Image style={styles.menuButtonImage} source={tileFive} />
              <Text style={styles.menuButtonLabel}>Rewards</Text>
            </TouchableOpacity>
          </Col>
          <Col xs={4}>
            <TouchableOpacity onPress={() => navigation.navigate('Map')}>
              <Image style={styles.menuButtonImage} source={tileSix} />
              <Text style={styles.menuButtonLabel}>Battle</Text>
            </TouchableOpacity>
          </Col>
        </Row>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#242424',
  },
  playerContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  playerImage: {
    height: 200,
    width: 200,
  },
  playerLevel: {
    fontFamily: 'Romulus',
    fontSize: 25,
    color: '#fff',
    position: 'absolute',
    top: 150,
  },
  playerName: {
    fontFamily: 'Romulus',
    fontSize: 25,
    color: '#fff',
    position: 'absolute',
    bottom: 150,
  },
  menuButtonImage: {
    height: 80,
    width: 80,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuButtonLabel: {
    background: 'pink',
    fontFamily: 'Romulus',
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
  },
});

HomeScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

HomeScreen.defaultProps = {
  navigation: {},
};

export default HomeScreen;
