import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
  // Dimensions,
} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import PropTypes from 'prop-types';

// const { height, width } = Dimensions.get('window');

const barbarian = require('../../assets/character/barbarian/barbarian_1_walk.gif');

const map = require('../../assets/map/map1.jpg');
const map2 = require('../../assets/map/map2.png');
const map3 = require('../../assets/map/map3.png');
const map4 = require('../../assets/map/map4.png');

// https://simplemmo.fandom.com/wiki/Stats

const MapScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.playerContainer}>
        <Image style={styles.playerImage} source={barbarian} />
      </View>
      <View style={{ flex: 1 }}>
        <Row>
          <Col xs={12}>
            <TouchableOpacity onPress={() => navigation.navigate('Fight', { map: map })}>
              <View style={styles.mapContainer}>
                <Image style={styles.mapImage} source={map} />
                <Text style={styles.mapLabel}> Haunted Forest </Text>
                <Text style={styles.mapLevel}> Lv 1 - 20</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Fight', { map: map2 })}>
            <View style={styles.mapContainer}>
              <Image style={styles.mapImage} source={map2} />
              <Text style={styles.mapLabel}> Desert Storm </Text>
              <Text style={styles.mapLevel}> Lv 20 - 40</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Fight', { map: map3 })}>
            <View style={styles.mapContainer}>
              <Image style={styles.mapImage} source={map3} />
              <Text style={styles.mapLabel}> Enchanted River </Text>
              <Text style={styles.mapLevel}> Lv 40 - 60</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Fight', { map: map4 })}>
            <View style={styles.mapContainer}>
              <Image style={styles.mapImage} source={map4} />
              <Text style={styles.mapLabel}> Ocean Deep </Text>
              <Text style={styles.mapLevel}> Lv 70 - 80</Text>
            </View>
            </TouchableOpacity>
          </Col>
        </Row>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#242424',
  },
  playerContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  playerImage: {
    height: 200,
    width: 200,
  },
  playerName: {
    fontFamily: 'Romulus',
    fontSize: 30,
    color: '#fff',
  },
  menuButtonImage: {
    height: 80,
    width: 80,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stats: {
    background: 'pink',
    fontFamily: 'Romulus',
    color: '#fff',
    textAlign: 'center',
  },
  statsLabel: {
    fontSize: 25,
  },
  statsValue: {
    fontSize: 30,
  },
  mapContainer: {
    marginBottom: 15
  },
  mapLevel: {
    fontFamily: 'Romulus',
    fontSize: 20,
    position: 'absolute',
    top: 5,
    right: 5,
    color: '#fff',
    backgroundColor: '#242424f0',
  },
  mapLabel: {
    fontFamily: 'Romulus',
    fontSize: 20,
    position: 'absolute',
    bottom: 0,
    color: '#fff',
    backgroundColor: '#242424f0',
  },
  mapImage: {
    // width: 'auto',
    height: 100,
    borderWidth: 3,
    borderColor: '#000',
  },
});

MapScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

MapScreen.defaultProps = {
  navigation: {},
};

export default MapScreen;
