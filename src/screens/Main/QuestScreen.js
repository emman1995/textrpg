import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  // Dimensions,
} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import PropTypes from 'prop-types';

const QuestScreen = ({ navigation }) => {
  const [setModalVisible] = useState(false);
  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        <Row>
          {
               [...Array(16)].map((val, item) => {
                 return (
                   <Col
                     xs={6}
                   >
                     <TouchableOpacity onPress={() => setModalVisible(true)}>
                       <View style={styles.questContainer}>
                         <Text
                           style={[
                             styles.questTitle,
                           ]}
                         >
                           Quest #
                           {item}
                         </Text>
                         <Text style={[styles.questSubtitle]}>Location</Text>
                       </View>
                     </TouchableOpacity>
                   </Col>
                 );
               })
          }
          <Col
            xs={6}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats]}>Daily Quest</Text>
          </Col>
          <Col
            xs={6}
            style={{
              backgroundColor: 'red',
            }}
          >
            <Text style={[styles.stats]}>Weekly Quest</Text>
          </Col>
        </Row>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#242424',
  },
  playerContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  questContainer: {
    borderWidth: 1,
    borderColor: '#ff0000',
    margin: 5,
  },
  questTitle: {
    fontSize: 25,
    textAlign: 'center',
    fontFamily: 'Romulus',
    color: 'white',
    marginBottom: 5,
  },
  questSubtitle: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'Romulus',
    color: 'white',
    marginBottom: 7,
  },
  playerImage: {
    height: 200,
    width: 200,
  },
  playerName: {
    fontFamily: 'Romulus',
    fontSize: 30,
    color: '#fff',
  },
  menuButtonImage: {
    height: 80,
    width: 80,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stats: {
    background: 'pink',
    fontFamily: 'Romulus',
    color: '#fff',
    textAlign: 'center',
  },
  statsLabel: {
    fontSize: 25,
  },
  statsValue: {
    fontSize: 30,
  },
});

QuestScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

QuestScreen.defaultProps = {
  navigation: {},
};

export default QuestScreen;
