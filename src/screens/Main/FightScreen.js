import React, { useEffect, useState, useRef } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  Animated,
  Easing,
} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import PropTypes from 'prop-types';

const { height, width } = Dimensions.get('window');

const barbarian = require('../../assets/character/barbarian/barbarian_1_walk.gif');
const barbarianHit = require('../../assets/character/barbarian/barbarian_1_hit.gif');

const wizard = require('../../assets/character/wizzard/wizard_1_walk.gif');
const wizardHit = require('../../assets/character/wizzard/wizard_1_hit.gif');

// const map = require('../../assets/map/map1.jpg');
// const map2 = require('../../assets/map/map2.png');
// const map3 = require('../../assets/map/map3.png');
// const map4 = require('../../assets/map/map4.png');

// https://simplemmo.fandom.com/wiki/Stats

const FightScreen = ({ route, navigation }) => {
  const [widthTimer, setWidthTimer] = useState(0);
  const [spawnEnemy, setSpawnEnemy] = useState(true);
  const [playerImage, setPlayerImage] = useState(barbarian);
  const [enemyImage, setEnemyImage] = useState(wizard);

  const [battleLogs, setBattleLogs] = useState([])

  const [playerHP, setPlayerHP] = useState(3000);
  const [enemyHP, setEnemyHP] = useState(3000);
  const playerAnim = useRef(new Animated.Value(0)).current;
  const enemyAnim = useRef(new Animated.Value(0)).current;

  const { map } = route.params;

  useEffect(() => {
    // const timer = setInterval(() => {
    //   if (widthTimer <= 300) {
    //     setWidthTimer((prevData) => prevData + 15);
    //   } else {
    //     setWidthTimer(300)
    //   }
    // }, 1000);

    // return () => {
    //   clearInterval(timer);
    // };

    // randomSpawn();
    const fighty = setInterval(() => {
      playerAttackAnimation();
      enemyAttactAnnimation();
    }, 4000);

    // return () => {
    //   clearInterval(fighty);
    // };
  }, [playerAnim]);

  const playerAttackAnimation = () => {
    Animated.timing(
      playerAnim,
      {
        toValue: width / 4,
        duration: 1500,
        useNativeDriver: true,
      },
    ).start(() => {
      const damage = (Math.floor(Math.random() * 50) + 10);

      setEnemyImage(wizardHit);
      setEnemyHP((prevData) => prevData - damage);
      setBattleLogs(prevArray => [...prevArray, { type: 'Enemy', dmg: damage }]);
      Animated.timing(
        playerAnim,
        {
          toValue: 0,
          easing: Easing.back(),
          duration: 1500,
          useNativeDriver: true,
        },
      ).start(() => {
        setEnemyImage(wizard);
      });
    });
  };

  const enemyAttactAnnimation = () => {
    Animated.timing(
      enemyAnim,
      {
        toValue: width / 4,
        duration: 1500,
        useNativeDriver: true,
      },
    ).start(() => {
      const damage = (Math.floor(Math.random() * 50) + 10);

      setPlayerImage(barbarianHit);
      setPlayerHP((prevData) => prevData - damage);
      setBattleLogs(prevArray => [...prevArray, { type: 'You', dmg: damage }]);
      Animated.timing(
        enemyAnim,
        {
          toValue: 0,
          easing: Easing.back(),
          duration: 1500,
          useNativeDriver: true,
        },
      ).start(() => {
        setPlayerImage(barbarian);
      });
    });
  };

  const randomSpawn = () => {
    if (!spawnEnemy) {
      const rand = Math.round(Math.random() * (20000 - 10000)) + 10000;
      setTimeout(() => {
        randomSpawn();
        setSpawnEnemy(true);
      }, rand);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.mapContainer}>
        <Image style={styles.mapImage} source={map} />
        <Animated.View
          style={{
            left: playerAnim,
          }}
        >
          <Image style={styles.mapPlayer} source={playerImage} />
        </Animated.View>
        <View style={[styles.mapTimer, { width: widthTimer }]} />
        {
          spawnEnemy
          && (
            <Animated.View
              style={{
                right: enemyAnim,
              }}
            >
              <Image style={styles.mapEnemy} source={enemyImage} />
            </Animated.View>
          )
        }

        <Row>
          <Col xs={6}>
            <Text style={[styles.stats, styles.statsValue]}>{playerHP}</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Player</Text>
          </Col>
          <Col xs={6}>
            <Text style={[styles.stats, styles.statsValue]}>{enemyHP}</Text>
            <Text style={[styles.stats, styles.statsLabel]}>Wizard</Text>
          </Col>
        </Row>

        <Row>
          <Col xs={12}>
            <Text style={[styles.stats, styles.statsValue]}>Battle Logs</Text>
          </Col>
          {
            battleLogs?.map((val, key) => {
              return (
                <Col xs={12}>
                  <Text>{val.type} Dmg. {val.dmg}</Text>
                </Col>
              );
            })
          }
        </Row>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#242424',
  },
  mapContainer: {
    marginBottom: 15,
  },
  mapTimer: {
    height: 10,
    width: 10,
    backgroundColor: 'yellow',
    position: 'absolute',
    top: 0,
  },
  mapImage: {
    // width: 'auto',
    height: 200,
    borderWidth: 3,
    borderColor: '#000',
  },
  mapPlayer: {
    height: 150,
    width: 150,
    position: 'absolute',
    bottom: 0,
  },
  mapPlayerHealth: {
    height: 5,
    width: 50,
    backgroundColor: 'red',
    position: 'absolute',
    bottom: 10,
    left: 30,
  },
  mapEnemy: {
    height: 150,
    width: 150,
    position: 'absolute',
    bottom: 0,
    right: 0,
    transform: [
      { scaleX: -1 },
    ],
  },
  stats: {
    background: 'pink',
    fontFamily: 'Romulus',
    color: '#fff',
    textAlign: 'center',
  },
  statsLabel: {
    fontSize: 25,
    color: '#e534eb',
  },
  statsValue: {
    fontSize: 30,
  },
});

FightScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  route: PropTypes.objectOf(PropTypes.any),
};

FightScreen.defaultProps = {
  navigation: {},
  route: {},
};

export default FightScreen;
