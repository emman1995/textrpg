import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Modal,
  TouchableOpacity
} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import PropTypes from 'prop-types';

// const { height, width } = Dimensions.get('window');
const bagIcon = require('../../assets/menu/tile005.png');
const swordIcon = require('../../assets/menu/tile007.png');
const armorIcon = require('../../assets/menu/tile000.png');
const bowIcon = require('../../assets/menu/tile001.png');
const bootsIcon = require('../../assets/menu/tile002.png');
const ringIcon = require('../../assets/menu/tile004.png');
const helmetIcon = require('../../assets/menu/tile008.png');

// https://simplemmo.fandom.com/wiki/Stats
const color = ['#e534eb', '#eb8f34', '#34a2eb', '#fff'];
const itemSample = [swordIcon, armorIcon, bowIcon, bootsIcon, ringIcon, helmetIcon];

const BagScreen = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <>
      <View style={styles.container}>
        <View>
          <Row>
            {
              [...Array(10)].map((val, item) => {
                return (
                  <Col
                    xs={6}
                  >
                    <TouchableOpacity onPress={() => setModalVisible(true)}>
                      <View style={styles.itemContainer}>
                        <Image
                          style={styles.itemImage}
                          source={itemSample[Math.floor(Math.random() * itemSample.length)]}
                        />
                        <Text
                          style={[
                            styles.stats,
                            styles.itemName,
                            { color: color[Math.floor(Math.random() * color.length)] },
                          ]}
                        >
                          Item #
                          {item}
                        </Text>
                        <Text style={[styles.stats, styles.itemAttributes]}>Atk 50-100</Text>
                      </View>
                    </TouchableOpacity>
                  </Col>
                );
              })
            }
          </Row>
        </View>
      </View>
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <TouchableOpacity style={styles.centeredView} onPress={() => setModalVisible(false)}>
          <View style={styles.modalView}>
            <Row>
              <Col xs={12}>
                <View style={styles.itemContainer}>
                  <Image
                    style={styles.itemImage}
                    source={itemSample[Math.floor(Math.random() * itemSample.length)]}
                  />
                  <Text
                    style={[
                      styles.stats,
                      styles.itemName,
                      { color: color[Math.floor(Math.random() * color.length)] },
                    ]}
                  >
                    Item #
                  </Text>
                  <Text style={[styles.stats, styles.itemAttributes]}>Atk 50-100</Text>
                </View>
              </Col>
            </Row>
          </View>
        </TouchableOpacity>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#242424',
  },
  playerContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  playerImage: {
    height: 200,
    width: 200,
  },
  playerName: {
    fontFamily: 'Romulus',
    fontSize: 30,
    color: '#fff',
  },
  itemContainer: {
    borderWidth: 5,
    borderColor: '#000',
  },
  itemImage: {
    height: 70,
    width: 70,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stats: {
    background: 'pink',
    fontFamily: 'Romulus',
    color: '#000',
  },
  itemName: {
    fontSize: 25,
    textAlign: 'center',
  },
  itemAttributes: {
    fontSize: 20,
  },
  centeredView: {
    flex: 1,
    backgroundColor: '#242424f0',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
  },
});

BagScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

BagScreen.defaultProps = {
  navigation: {},
};

export default BagScreen;
