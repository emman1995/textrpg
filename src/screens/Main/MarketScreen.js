import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  TouchableOpacity,
  FlatList,
  // Dimensions,
} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import PropTypes from 'prop-types';
import { Tab, TabView } from 'react-native-easy-tabs';

// const { height, width } = Dimensions.get('window');

const barbarian = require('../../assets/character/barbarian/barbarian_1_walk.gif');

// const { height, width } = Dimensions.get('window');
const bagIcon = require('../../assets/menu/tile005.png');
const swordIcon = require('../../assets/menu/tile007.png');
const armorIcon = require('../../assets/menu/tile000.png');
const bowIcon = require('../../assets/menu/tile001.png');
const bootsIcon = require('../../assets/menu/tile002.png');
const ringIcon = require('../../assets/menu/tile004.png');
const helmetIcon = require('../../assets/menu/tile008.png');

// https://simplemmo.fandom.com/wiki/Stats
const color = ['#e534eb', '#eb8f34', '#34a2eb', '#fff'];
const itemSample = [swordIcon, armorIcon, bowIcon, bootsIcon, ringIcon, helmetIcon];

// https://simplemmo.fandom.com/wiki/Stats

const MarketScreen = ({ navigation }) => {
  const [currentTab, setCurrentTab] = useState(0);
  return (
    <View style={styles.fill}>
      <View style={styles.padding}>
        <Row>
          {
            itemSample.map((val, key) => {
              return (
                <TouchableOpacity onPress={() => setCurrentTab(key)}>
                  <Col xs={3}>
                    <Image
                      style={styles.menuImage}
                      source={val}
                    />
                  </Col>
                </TouchableOpacity>
              );
            })
          }
        </Row>
      </View>
      <TabView selectedTabIndex={currentTab}>
        {
          itemSample.map((val, key) => {
            return (
              <Tab>
                <View style={styles.container}>
                  <Row>
                    {
                      [...Array(10)].map((val1, item1) => {
                        return (
                          <Col
                            xs={12}
                          >
                            <View style={styles.itemContainer}>
                              <View>
                                <Image
                                  style={styles.itemImage}
                                  source={val}
                                />
                              </View>
                              <View>
                                <Text
                                  style={[
                                    styles.stats,
                                    styles.itemName,
                                    { color: color[Math.floor(Math.random() * color.length)] },
                                  ]}
                                >
                                  Item #
                                  {item1}
                                </Text>
                                <Text style={[styles.stats, styles.itemAttributes]}>
                                  Atk 50-100
                                </Text>
                              </View>
                            </View>
                          </Col>
                        );
                      })
                    }
                  </Row>
                </View>
              </Tab>
            );
          })
        }
      </TabView>
    </View>
  );
};

const styles = StyleSheet.create({
  fill: {
    backgroundColor: '#242424',
  },
  container: {
    flex: 1,
    backgroundColor: '#242424',
  },
  playerContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  playerImage: {
    height: 200,
    width: 200,
  },
  playerName: {
    fontFamily: 'Romulus',
    fontSize: 30,
    color: '#fff',
  },
  menuButtonImage: {
    height: 80,
    width: 80,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stats: {
    background: 'pink',
    fontFamily: 'Romulus',
    color: '#fff',
    textAlign: 'center',
  },
  statsLabel: {
    fontSize: 25,
  },
  statsValue: {
    fontSize: 30,
  },
  itemContainer: {
    borderWidth: 2,
    borderColor: '#000',
    flexDirection: 'row',
    paddingTop: 20,
    paddingBottom: 20,
  },
  itemImage: {
    height: 40,
    width: 40,
  },
  itemName: {
    fontSize: 30,
  },
  itemAttributes: {
    fontSize: 20,
  },
  menuImage: {
    height: 60,
    width: 60,
    margin: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

MarketScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

MarketScreen.defaultProps = {
  navigation: {},
};

export default MarketScreen;
